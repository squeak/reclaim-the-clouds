const _ = require("underscore");
const $$ = require("squeak/node");
let appConfig = require("../boot/config");
const squidInterpret = require("squid/interpret");

/**
  DESCRIPTION: list of pages served
  TYPE: <pageRouteObject[]>
  TYPES:
    pageRouteObject = {
      !url: <string> « absolute url »
      ?redirect: <string> « if defined, instead of serving a page, this url will redirect to the given value »,
      ?name: <string> « if not defined will be autofilled by electrode with the last chunk of the pageRouteObject.url »,
      ?title: <string> « if not defined will be set to pageRouteObject.name's value »,
      ?path: <string> «
        path to script and styles folder (relative to the pages directory)
        normally you'd want to leave this to it's default value that will be automatically set from pageRouteObject.url (pageRouteObject.url.replace(/^\//, ""))
      »,
      ?data: <string> «
        path to pages data (assets)
        normally you'd want to leave this to it's default value (appConfig.browserPath.pagesData + pageRouteObject.path)
      »,
      ?auth: <{ ?users: <string[]>, ?roles: <string[]> }> «
        list of users and/or roles authorized to access this page
        !!! you need to have set boot/config/authentication.couchUrl for this to be used !!!
      »,
      // in addition to these keys, an additional "children" key will be created with a list of this page's children pages, to be passed to the page object in the browser
    }
*/
var pages = [

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INDEX PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  {
    url: "/",
    // name: "index",
    // path: "index/",
    // title: "Home",
    redirect: "/en/doc/",
  },

  {
    url: "/en/",
    redirect: "/",
  },

  // {
  //   url: "/fr/",
  // },

  {
    url: "/error/",
    title: "Error",
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DOC
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


  {
    url: "/doc/",
    redirect: "/en/doc/",
  },

  {
    path: "doc/",
    url: "/en/doc/",
    title: "Reclaim the clouds",
    moduleConfig: makePageDoc({
      title: "Reclaim the clouds",
      docDir: "wiki-en/",
    }),
  },

  // {
  //   path: "doc/",
  //   url: "/fr/doc/",
  //   title: "Reclaim the clouds",
  //   moduleConfig: makePageDoc({
  //     title: "À nous les nuages",
  //     docDir: "wiki-fr/",
  //   }),
  // },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

];

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makePageDoc (opts) {
  return squidInterpret($$.defaults({
    name: "reclaim-the-clouds",
    absolutePath: appConfig.appPath,
    logoUrl: "/share/rtc-logo.svg",
    backgroundWallpaper: "/share/wallpaper.png",
    docCopyright: "squeak (squeak [aATt] eauchat.org)",
    // relativeRequirePath:
    // moduleRelativePath: "/", or "." ??
  }, opts));
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pages;
