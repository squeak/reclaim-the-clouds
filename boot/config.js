const path = require("path");
const $$ = require("squeak/node");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET LOCAL CONFIG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let localConfig;
try { localConfig = require("../config/private"); }
catch (e) { localConfig = {}; };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULT CONFIG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var defaultConfig = {

  //
  //                              GENERAL

  name: "reclaim-the-clouds", // this will overwrite the name set in package.json, you can safely omit it if you're fine with that one
  port: 6564,

  logColor: ["#f4f4f4", "#2FE9C8", "#2FE96F"],

  appPath: path.join(__dirname +"/../"), // Important, don't change this unless you move this script.

  // set to true to precalculate all dist files on server start
  production: true,

  // for more debugging logs, enable this
  // debug: true,

  // calculate list of submodules to allow to easily find less style files in submodules <false|"always"|"once">
  resolveSubmodulesPathsRecursivelyForLessImportStatments: "once",

  //
  //                              ADVANCED OPTIONS

  // unquote the following, setup a couchdb instance and pass it as url here if you want to have some private pages requiring authentication
  // authentication: {
  //   couchUrl: "http://localhost:5984/", // choose here the url to the couchdb server containing your list of users (normally it's enough for it to be accessible from this app but no necessarily over the internet)
  // },

  // unquote the following, setup the list of pages and customUrls to cache, and if you want customize pwa manifest, to enable the possibility to install your app in mobile phones and use it offline
  // pwa: {
  //   cache: true,
  //   pages: [],
  //   customUrls: [],
  //   manifest: {
  //     icons: [
  //       {
  //         "src": "/some/image.png",
  //         "sizes": "500x500",
  //         "type": "image/png"
  //       },
  //     ],
  //     background_color: "#0088FF",
  //   },
  // },

  // NOTE: see https://framagit.org/squeak/electrode/blob/master/settings/defaultConfig.js for more custom options you can pass here
  // or have a look at electrode documentation for more information (https://squeak.eauchat.org)

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $$.defaults(defaultConfig, localConfig)
