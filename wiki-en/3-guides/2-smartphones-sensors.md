# Smartphones and sensors

A major difference between a computer and a smartphone, is that a smartphone contains a great amount of sensors that can track, record, and reveal many aspects of your private life. Usually smartphones can record:
 - locations (GPS)
 - sounds (microphones)
 - images (cameras)
 - acceleration and movement
 - compass
 - magnetic fields
 - light (luminosity sensors)

Smartphones can also usually communicate, send and receive all kinds of data distantly via:
 - GSM telephony networks
 - wifi networks
 - bluetooth
 - RFID chip readers (the kind of chips that are in credit card, id card, public transportation pass...)
