# Some best practices for linking to resources
*This guide comes from an article published by someone in Toestand's forum which seems to be unavailable now.*

### Never link directly to executables, ever:
Link to a page describing what it is instead.

### Do not use "shortened" links:
In most cases we should try not to post "shortened" links, and be wary of clicking on obfuscated links in general.
Protip: checking them before you click is a good idea anyway: `$ curl https://unshorten.me/s/{goo.gl/yzLPTN}`

### Linking to youtube content:
It’s courteous to non-Google fans to link to invidio.us or other instead of youtube:
https://invidio.us/watch?v={video_id}
http://axqzx4s6s54s32yentfqojs3x5i7faxza6xo3ehd4bzzsg2ii4fv2iid.onion/watch?v={video_id}
Alternate servers: instances.invidio.us

### Linking to twitter content:
For Twitter content consider using nitter.net instead; just replace “twitter.com” with “nitter.net” in the URL

### Removing tracking parameters from the urls:
Please try to remove tracking parameters from the end of querystrings, e.g. utm_source=GCHQ

### Avoid linking to website that track users:
Bonus points if you avoid linking to sites with aggressive profiling if the intent is read-only. For example, linking to a Google doc if the viewer is not going to /edit (readonly) - a better choice is to link to a pastebin site instead. Also for sites behind paywalls, try using archive.is to show an archived version

### Providing onion links (when they exist):
Feel free to offer onion links as alternatives in your post, some small number of people may click on those instead. DDG, NYT, Invidio.us, etc. - all mirror their sites on .onion domains
