
This site aims at documenting some alternatives to global mass surveillance and centralized technologies.

It's separated in three main sections, [learning](?learn), [finding alternatives](?alternatives), and [practicals guides](?guides).
The [learning section](?learn) contains sites, articles, conferences and other documents that describe why we need alternatives.
The [alternatives section](?alternatives) lists some good decentralized and more privacy aware tools we can use.
In the [guide section](?guides), you will find diverse practical guides that may be of some use in your digital life.

If you'd like to have a more visual approach, you can have a quick look a [this little story](?visual).

This site in no way aims to be exhaustive. It contains just a collection of findings gathered over time.
