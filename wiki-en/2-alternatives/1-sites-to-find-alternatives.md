# Sites that help you finding alternatives

- [Prism-break](https://prism-break.org/en/), find alternatives to services and apps you use.
- [Privacytools](https://www.privacytools.io/), a website with a lot of documentation and tools proposed.
- [Switching social](https://switching.social/).
- [Degooglisons internet](https://degooglisons-internet.org/), find alternatives to services and apps you use — *french*
- [Framalibre](https://framalibre.org/), an index of free and open source softwares — *french*
