# Alternatives to the internet

There are attempts to create alternative networks, to provide privacy respecting exchanges that the internet don't provide, here are listed a few developments:

- [Freenet](https://freenetproject.org/index.html), a peer-to-peer platform for censorship-resistant communication and publishing
- [I2P](https://geti2p.net/en/about/intro), The Invisible Internet Project
