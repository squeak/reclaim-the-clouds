# Find apps and softwares


## Chatting
- [All your communications in one app](https://www.caliopen.org/en/).


## Sharing big files
 - [Decentralized and anonymous (peer to peer) file sharing via Tor](https://onionshare.org/).


## Browsers
 - Firefox
 - Brave
 - Icecat (100% free firefox)
 - [browsers recommended by privacytools](https://www.privacytools.io/browsers/)


## Improving your browser

### Testing your browser fingerprint unicity:
- [Am I unique](https://amiunique.org/)
- [panopticlick](https://panopticlick.eff.org/)
- tracographie.org (but couldn't find it in fact, a map to explain visually all 3rd parties getting infos from you when you use some websites)

### Firefox addons:
- uBlock origin ([Ublock origin advanced user tutorial.](https://www.invidio.us/watch?v=2lisQQmWQkY))
- Privacy badger
- Https everywhere
- Decentraleyes
- [Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)
- Random User-Agent (maybe) / User-Agent Switcher and manager
- Flagfox
- [CanvasBlocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/) (maybe)
- Lightbeam - To see a map of third parties connections
- Browserpass
- [trace](https://addons.mozilla.org/en-US/firefox/addon/absolutedouble-trace/) (maybe)
- [trackmenot](https://addons.mozilla.org/fr/firefox/addon/trackmenot/)
- [adnauseam](https://addons.mozilla.org/fr/firefox/addon/adnauseam/)
- [browser addons recommended by privacytools](https://www.privacytools.io/browsers/#addons)


## Android apps
- [F-Droid](https://f-droid.org/), an app store that proposes only free and open source applications.
- [K9 mail](https://k9mail.app/), available in F-Droid, a mail client compatible with most email providers.
- [Password-store](https://f-droid.org/en/packages/com.zeapo.pwdstore/), a password manager compatible with pass (see below in the "Password managers" section).
- [Blokada](https://blokada.org/index.html), an app to prevent your phone from connecting to sites you don't want to allow.
- If you want to install [Signal](https://signal.org/) or [Firefox](https://www.mozilla.org/en-US/firefox/new/) and get automatic updates, you can install it through F-Droid. To do so, you will need to and an extra F-Droid repository. To do so:
  * Open F-droid, go to settings –> Repositories and then add (via the plus "+" button on the top-right corner) the following custom list in the `repository address field`: `https://rfc2822.gitlab.io/fdroid-firefox/fdroid/repo` (just type this text in)
  * Then go back to F-Droid and swipe down to trigger the refreshing of the app list, you can now find signal and firefox in the list.


## Encryption:
- Gnupg, Veracrypt (for files)
- LUKS (for whole disk, the easiest way is to set it up when installing your system)


## Password managers:
- [keepassxc](https://keepassxc.org/), [guide on how to use it](https://ssd.eff.org/en/module/how-use-keepassxc)
- [pass](https://www.passwordstore.org/), a very nice password manager, in the terminal, based on standard technologies (gpg+git), (with autofilling of login forms in browser using [this plugin](https://github.com/browserpass/browserpass-extension), and [a compatible android app](https://f-droid.org/en/packages/com.zeapo.pwdstore/))
