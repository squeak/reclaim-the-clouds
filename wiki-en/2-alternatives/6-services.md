# Find services


## Emails providers
- [Riseup](https://riseup.net/) (USA/Germany?)
- [Autistici/Inventati](https://www.autistici.org/) (Italy)
- [Disroot](https://disroot.org/en/services/email) (The netherlands)
- [Webarchitects](https://www.webarchitects.coop/email) (UK) - PAY
- [Tutanota](https://tutanota.com/) (Germany)
- [Protonmail](https://protonmail.com/) (Switzerland/USA?)
- see also the list of hosters in the previous section, many of them provide email


## Search engines
- [cliqz](https://cliqz.com/) (based in Germany)
- [duckduckgo](https://duckduckgo.com/) (based in the USA)


## Audio and video conferences

### Jitsi (audio and video in the browser):
- [videodulibre](https://jitsi.videodulib.re/) (in Toulouse, France)
- [chapril](https://visio.chapril.org/), proposed by April (a french organization promoting free/libre software since 1996)
- [jitsi meet](https://meet.jit.si/), proposed by the creators of jitsi (in the USA)
- [list of open instances to use jitsi](https://lafibre.info/navigateurs/visioconference/)

### Multiparty meeting (should work even with bad internet connection)
- [letsmeet](https://letsmeet.no/), proposed by the creators of multiparty-meeting
- [riminilug](https://iorestoacasa.riminilug.it) (associazione culturale in Rimini, Italy)
- [Consiglio Nazionale delle Ricerche](https://mm.cedrc.cnr.it)

### Mumble (good for group calls with many person involved, audio only):
- [le-pic web mumble](https://audio.le-pic.org/) (in Toulouse, France)
- [mumble web interface demo](https://voice.johni0702.de/?address=voice.johni0702.de&port=443/demo)
- [framasoft web mumble](https://web.mumble.framatalk.org/)
- [documentation for using mumble](https://wiki.chatons.org/doku.php/la_conference_telephonique_avec_mumble) (by chatons)

### Big blue button (remote teaching for classes):
- [faimaison](https://bbb.faimaison.net/)
- [tetaneutral](https://bbb1.tetaneutral.net/html5client/) (didn't manage to make it work)


## Chatting
- **MATRIX/RIOT**: find instances on [the matrix federation](https://the-federation.info/matrix|synapse), or on [privacytools list](https://riot.privacytools.io/#/welcome).
- [ricochet](https://ricochet.im/) (never tested)
- [rocket.chat](https://rocket.chat/) (never tested)


## Social networks
- [Yay, the fediverse :)](https://fediverse.party/), an overview of the new interconnected decentralized social medias. And another page on [alternative social medias](https://switching.social/ethical-alternatives-for-advanced-users/).
- **MASTODON**: You can use [the official mastodon instance](https://mastodon.social/), or find [other mastodon instances](https://instances.social/), [and apps](https://joinmastodon.org/apps)
- **FRIENDICA**: [find friendica instances](https://dir.friendica.social/servers)


## Uploading and sharing videos
- **PEERTUBE**: [find peertube instances](https://instances.joinpeertube.org/instances).
- **MEDIAGOBLIN**: [official instance](https://goblinrefuge.com/mediagoblin/).
- **POLYMNY**: [an online tool create explanation videos from a pdf and recording an audio (useful for recording online courses)](https://polymny.studio/).


## Sending files
- [Lufi, sans-nuages](https://drop.sans-nuage.fr/)

## Car sharing
- [mobicoop](https://www.mobicoop.fr/)
- [trajetalacarte](https://www.trajetalacarte.com/) (maybe not a libre wesite)


## Interactive and collaborative maps
- [framacarte](https://framacarte.org/en/)
- [umap](https://umap.openstreetmap.fr/)
- [facilmap](https://facilmap.org)


## DNS over https
- by FFDN, address to use: https://ldn-fai.net/dns-query
- [by 42L (Lyon)](https://42l.fr/DoH-service), address to use: https://doh.42l.fr/dns-query
