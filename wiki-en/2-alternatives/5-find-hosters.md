# Hosters

The following hosters propose access to services like email, cloud, website hosting, collaborative documents edition, decentralized social networks, chatting and voice conversations...
They all have different conditions on who can create accounts on their platforms, and prices they charge for their services, but most of the ones listed here are free, welcoming and care about privacy.

| name | where | languages | pricing | structure type | is a [chaton](https://chatons.org/) | comments |
| :--: | :---: | :-------: | :-----: | :------------: | :---------------------------------: | :------: |
| [riseup](https://riseup.net/) | . | many languages | free (donation based) | non-profit |  ✖ | made to support activists |
| [framasoft](https://framasoft.net/)  | France | many languages | free (donation based) | non-profit | ✔ | - |
| [faimaison](https://www.faimaison.net/)  | Nantes, France | french | ? | non-profit | ? | - |
| [domaine public](https://domainepublic.net/) | Belgium | french | ? | non-profit | ✔ | - |
| [disroot](https://disroot.org/en) | Netherlands | many languages | ? | non-profit | ✖ | - |
| [autistici](https://autistici.org) | . | many languages | free (donation based) | non-profit | ✖ | made to support activists |
| [42L](https://42l.fr/) | Lyon, France | french and english | free/pay | non-profit | ✔ | - |
| [eauchat](https://eauchat.org/) | Toulouse, France | french, english, italian and spanish | free (donation based) | non-profit | soon | - |
| [sans-nuages](https://sans-nuage.fr/) | Alsace, France | french | pay | non-profit | ✔ | - |
| [allmende](https://allmende.io/) | Witzenhausen, Germany | english/german | ? | ? | ✖ | - |
| [ethibox](https://ethibox.fr/) | Nancy, France | french | pay | micro-enterprise | ✔ | - |
| [colibris](https://www.colibris-outilslibres.org/) | France | french | ? | non-profit | ? | - |
| [systemli](https://www.systemli.org/en/index.html) | Germany? | ? | ? | ? | ? | - |
| [wehost](https://weho.st/) | Netherlands | ? | ? | ? | ? | - |
| [maadix](https://maadix.net/) | ? | english, spanish, catalan | ? | ? | ✖ | - |
| [xnet](https://xnet-x.net/en/) | ? | ? | ? | ? | ✖ | - |
| [nubo](https://nubo.coop/fr/) | Belgium | french, dutch | pay | collective/enterprise | ✖ | - |
| [l'autre](https://www.lautre.net/) | France | french | ? | ? | ? | - |
| [koumbit](https://www.koumbit.org) | Montréal, Québec | french, english | free/pay | collective | ✔ | - |
| [webarchitects](https://www.webarchitects.coop/) | Sheffield, UK | english | pay | ? | ? | - |
| [darkpeak](https://darkpeak.org/) | UK | english | ? | ? | ? | - |
| [zaclys](https://www.zaclys.com) | France | french | ? | business-enterprise | ✔ | also friendly [for schools](https://www.zaclys.com/zaclys-offre-lor-aux-ecoles-primaires-et-plus/))  |
| [flap](https://www.flap.cloud/) | France | french | pay | business-enterprise | ✔ | - |
| [woelkli](https://woelkli.com/) | Switzerland | english, french, german | free/pay | business-enterprise | ? | - |
| [cisti](https://cisti.org/) | ? | italian | ? | ? | ✖ | - |
| [drycat](https://www.drycat.fr/) | Nantes, France | french | free (donation based) | individual | ? | - |
| [greli.net](https://greli.net/) | Pau, France | french | free (donation based) | non-profit | ✔ | - |
| [girofle.cloud](https://girofle.cloud/) | France | french | free (donation based) | non-profit | ✔ | - |
| [trom.tf](https://trom.tf) | ? | many languages | free (donation based) | non-profit | ? | - |
| [stemy](https://stemy.me/accueil/services) | Belgium | french | free | individual | ? | - |
| [deuxfleurs](https://deuxfleurs.fr/) | France | french | ? | non-profit | ✔ | - |

Two good place to look for alternative hosters are the [chatons federation](https://chatons.org) (mostly in French speaking countries), and [the libre hosters network](https://libreho.st/).  
You may also be interested in [this list](https://riseup.net/en/security/resources/radical-servers) of "radical servers", that riseup is advising.


## Or if you want to do it yourself ;)
You can also make your own server. Now, with the internet cube, it's quite simple.  
You can get some documentation [here](https://internetcu.be/).

To get further, here is a guide on [how to choose a VPS](https://no-google.frama.wiki/libre:vps).
And Neutrinet proposes a [cube install script here](https://git.domainepublic.net/Neutrinet/neutrinet_cube_install).
