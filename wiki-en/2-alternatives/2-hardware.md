# What computer/phone to buy/get


## Computers
- [very cheap computers](https://www.pine64.org/pinebook/)
- [more expensive but higher quality computers](https://puri.sm/products/librem-13/)
- [laptops and desktop computers with linux preinstalled](https://ubuntushop.be)
- [commown](https://shop.commown.coop/shop/category/ordinateurs-2)

If you're very concerned about global surveillance, use a computer without processor backdoor == no Intel Management Engine == before 2007. You can [buy some here](https://shop.nitrokey.com/shop/product/nitropad-x230-67).


## Phones
- [/e/](https://e.foundation/), buy second hand phones with a non-google version of android preinstalled.
- [Fairphone](https://shop.fairphone.com/fr/), fairtrade android phone ([other link](https://commown.coop/fairphone-3/)).
- [Librem](https://librem.one/#bundles), high quality phones with a free system preinstalled.
