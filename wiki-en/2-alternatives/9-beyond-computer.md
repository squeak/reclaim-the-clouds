# Beyond the computer


## Exchanges and alternative currencies
- [learn about the g1 currency](https://g1.duniter.fr/#/app/account)
- [a platform to exchange in g1](https://www.gchange.fr/#/app/home)
- [announces in g1](https://gannonce.duniter.org/#/)
- [find people and activities near you](https://framacarte.org/fr/map/monnaie-libre-g1_8702)


## Political attempts
- [mysociety](https://www.mysociety.org/)
