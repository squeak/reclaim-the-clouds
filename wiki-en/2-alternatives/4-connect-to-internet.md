# What operator to use to connect to internet

## Alternative internet providers in France
- [One that is part of the ffdn federation](https://www.ffdn.org/)
- [franciliens](https://www.franciliens.net/) — Paris region, France
- [tetaneutral](https://tetaneutral.net/) — Toulouse, France
- [ilico](https://www.ilico.org/) — Corrèze, France
- [ARN](https://arn-fai.net/) — Alsace, France
- search a bit if you can find one where you are, there are many in lots of different regions


## In Spain
- [guifi](https://guifi.net/)


## In Italy
- [ninux](http://ninux.org/)


## Use a VPN
If there is no alternative internet providers where you live, another option is to use a VPN, like that, at least, your internet provider will not be able to know the list of sites you visit.
You can for example use associative VPN providers like [neutrinet](https://neutrinet.be/en/vpn).
A list of privacy oriented VPN services is proposed by privacytools [here](https://www.privacytools.io/providers/vpn/).


## Create your own mesh network
- [Libre Router](https://librerouter.org/), a free hardware and software router.
