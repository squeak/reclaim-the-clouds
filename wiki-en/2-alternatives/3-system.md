# What operating system to install on it


## Computer:
- A very simple, easy to use and common linux distribution is [ubuntu](https://www.ubuntu.com/).
- If your computer is old or slow, you can try [lubuntu](https://lubuntu.net/) or [xubuntu](https://xubuntu.org) which are very light.
- [ElementaryOS](https://elementary.io/), linux with simple and sleek interface.


## Phone
- [LineageOS](https://lineageos.org/), a libre version of android.
- [/e/](https://e.foundation/), an easy and simple version of android (you can also buy from them second hand phones with it already installed).
- [Copperhead OS](https://copperhead.co/android/), a security and privacy focused mobile operating system compatible with Android apps.
- [GrapheneOS](https://grapheneos.org/), an open source privacy and security focused mobile OS with Android app compatibility.
- [Replicant](https://www.replicant.us/), a fully free Android distribution, this means that if some part of your phone don't have free drivers, they will just not work.
- Phone systems not based on android: [Plasma Mobile](https://www.plasma-mobile.org/), [PostmarketOS](https://postmarketos.org/), [Ubuntu Touch](https://ubuntu-touch.io/)...
