# Crazy control-freak companies

While facebook is gently letting you planning about what your social life should become when you die...

![](/pages/doc/your-death-with-facebook.jpg)

...the EU is giving away in 2020, 100 millions euros for financing research about biometric security. They offer 5 millions for each project organized from the cooperation of university and industrial companies.

The aim of those projects should be to:
1- Authentify id cards
2- Recognize that the person present is the one on the id document
3- Check if this person is in some specified databases
4- Detect suspicious profiles and people

And in the meantime...

![](/pages/doc/youtube-privacy-warning.jpg)

...you won't watch youtube if you refuse to be tracked...

![](/pages/doc/youku.jpg)

...and the chinese youtube, is doing strange things with you.

But everything is good, nothing to worry, because google just bought waze to [take you to the best place they like](/pages/doc/waze.mp4)...

[![see the site's video](/pages/doc/waze.jpg)](/pages/doc/waze.mp4)

...and all this is possible because there's just been one more of this tiny faceboogle datacenter built last month. (Or was it this month?)

![](/pages/doc/one-more-faceboogle-datacenter.jpg)

But what is google anyway, just a nicely designed search engine, no?
> Google controls about [62% of mobile browsers](https://gs.statcounter.com/browser-market-share/mobile/worldwide), [69% of desktop browsers](https://gs.statcounter.com/browser-market-share/desktop/worldwide), and the operating systems on [71% of mobile devices](https://www.netmarketshare.com/operating-system-market-share.aspx?options={"filter"%3A{"%24and"%3A[{"deviceType"%3A{"%24in"%3A["Mobile"]}}]}%2C"dateLabel"%3A"Trend"%2C"attributes"%3A"share"%2C"group"%3A"platform"%2C"sort"%3A{"share"%3A-1}%2C"id"%3A"platformsMobile"%2C"dateInterval"%3A"Monthly"%2C"dateStart"%3A"2019-02"%2C"dateEnd"%3A"2020-01"%2C"segments"%3A"-1000"}) in the world. [92% of internet searches](https://gs.statcounter.com/search-engine-market-share) go through Google and [73% of American adults](https://www.businessofapps.com/data/youtube-statistics/) use YouTube. Google runs code on approximately [85% of sites on the Web](https://w3techs.com/technologies/details/ta-googleanalytics) and inside [as many as 94%](https://www.statista.com/statistics/1035623/leading-mobile-app-ad-network-sdks-android/) of apps in the Play store. It collects data about users’ every click, tap, query, and movement from all of those sources and more.

![](/pages/doc/leave-everything-to-us.jpg)
