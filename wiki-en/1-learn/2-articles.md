# Articles and conferences


## Articles that explain well the problem and current situation
- [is it possible to be anonymous on the internet in 2019?](http://www.socialter.fr/fr/module/99999672/801/obfuscation__est_il_possible_de_se_camoufler_sur_internet_en_2019) — *french*
- [about surveillance capitalism](https://www.monde-diplomatique.fr/2019/01/ZUBOFF/59443) — *french*
- [how facebook will censor black children but not white men](https://www.propublica.org/article/facebook-hate-speech-censorship-internal-documents-algorithms)
- [mapping the problem: news dissemination on online platforms (Privacy Camp 2019)](https://invidio.us/watch?v=0TLTyiZWP3I), a very good conversation about how news are chosen, manipulated, and used by social media platforms, how their main focus with news is profit, and how this situation is used to manipulate populations in the political interest of platforms and states
- [about NSA's global surveillance and Snowden's revelations](https://blog.cryptographyengineering.com/2019/09/24/looking-back-at-the-snowden-revelations/)

In italian:
 - [Autodifesa digitale](https://facciamo.cisti.org)
 - [Guida all'autodifesa digitale](https://numerique.noblogs.org/)
 - [Brucia il telefono](https://bruciailtelefono.noblogs.org/files/2021/04/brucia_il_telefono.pdf)


## Creating alternative ways of thinking the tools we use, imagine and build better worlds
- [what is a free software](https://www.gnu.org/philosophy/free-sw.html), and why [non free software is often malware](https://www.gnu.org/proprietary/proprietary.en.html) — *both available in many languages*
- [contributopia](https://contributopia.org/en/essaimage/), *[french version](https://contributopia.org/fr/essaimage/)*
- [deframasoftisons internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) — *french*
- [videos to learn about yunohost and the internet cube](https://cinema.yunohost.support/)
- [alternative social networks](https://framablog.org/2019/03/07/la-fee-diverse-deploie-ses-ailes/) — *french*
- [defend public and democratic space](https://framablog.org/2019/11/22/une-coalition-francaise-pour-la-defense-et-la-promotion-de-lespace-democratique/#2019/11/19/) — *french*
- [How HTTPS and Tor Work Together to Protect Your Anonymity and Privacy](https://www.eff.org/pages/tor-and-https)
- About [users fundamental rights](https://udm.branchable.com/), and [on the same theme in french](https://framablog.org/2019/08/29/un-manifeste-des-donnees-utilisateurs-aujourdhui/)


## The old and new ways used by advertisement companies to track users
- [Google Says It Doesn’t 'Sell' Your Data. Here’s How the Company Shares, Monetizes, and Exploits It.](https://www.eff.org/deeplinks/2020/03/google-says-it-doesnt-sell-your-data-heres-how-company-shares-monetizes-and)
- [Google FLoC, please make my browser collect everything I do, and tell what I like to every website I visit](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea)


## How cities are being invaded with (surveillance) technologies
- A platform to [study the impact of technologies on cities](https://technopolice.fr/). — *french*
- [Une ville vivable](https://journals.openedition.org/terminal/4225) — *french*
- How google has huge control over [how we perceive the world and public space](https://visionscarto.net/espace-public-vs-google). — *french*


## Legal aspects / Legal fights
- Two very good articles on facial recognition in France: [how it's dangerous](https://www.laquadrature.net/2019/11/22/reconnaissance-faciale-le-bal-des-irresponsables/) and how [it's already used](https://www.laquadrature.net/2019/11/18/la-reconnaissance-faciale-des-manifestants-est-deja-autorisee/) — *both articles in french*
- [The right to data portability (and why it's a very bad idea)](https://archive.fosdem.org/2019/schedule/event/gdpr_and_dtp_vs_data_portability_and_freedom/) a talk on GDPR and how it doesn't do what we should expect from legislations.
- [Privacy at the US border](https://www.eff.org/wp/digital-privacy-us-border-2017#part-2).
- [EU facial recognition database](https://theintercept.com/2020/02/21/eu-facial-recognition-database/), [and more on it here as well](https://francais.rt.com/international/71582-reconnaissance-faciale-bientot-base-donnees-unique-echelle-europeenne).
- [How the french police ask ISPs to collaborate with them](https://neutrinet.be/en/blog/collabo). — *french*


## Technology, justice and prisons
- [Is smartphone tracking a less intrusive reward for good behaviour or just a way to enrich the incarceration industry?](https://www.theguardian.com/global-development/2021/mar/04/they-track-every-move-how-us-parole-apps-created-digital-prisoners)


## The specific issue with 5G
Yay, let's destroy the world a bit more with 5G!
- [Why 5G is baaaad.](https://www.bortzmeyer.org/5g.html) — *french*
- [5G a central piece of future military technologies](https://www.zeit-fragen.ch/fr/archives/2020/n-21-29-septembre-2020/le-cote-obscur-de-la-5g-lutilisation-militaire.html) — *french*
- [A NATO article that describes the 5G "challenge"](https://www.nato.int/docu/review/articles/2020/09/30/nato-and-the-5g-challenge/index.html)
- [An article that describes how 5G is crucial in military development.](https://www.defenseone.com/ideas/2020/08/nato-must-move-out-smartly-5g/167687/)
- [Thales, a french company that produces military equipment present their new 5G enabled technology.](https://www.thalesgroup.com/fr/des-operations-militaires-ultra-connectees-grace-aux-solutions-4g5g-lte) — *french*


## Miscellaneous
- [il faut dire crypter ou chiffrer? ;)](https://chiffrer.info/) — *french*
- [an atlas of the internet](https://louisedrulhe.fr/internet-atlas/)
