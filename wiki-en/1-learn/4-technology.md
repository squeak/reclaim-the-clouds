# How technologies we use work


## Phone calls
- [How does mobile phone calls work?](https://invidious.fdn.fr/watch?v=1JZG9x_VOwA) ([same video in youtube](https://www.youtube.com/watch?v=1JZG9x_VOwA))


## Browsing the internet
- [What happens when I visit a website](https://github.com/alex/what-happens-when), a fully detailed explanation of all the technical things that happen when you make a web search or visit a website. From the electric signal of the pressed key sent to the computer's kernel, to the display of a webpage content, passing by the circulations of data packets in the wires.


## General knowledge about computers and networking (windows 95 aesthetics videos, but fairly instructive ;))
- [What is a Proxy Server](https://yewtu.be/watch?v=5cPIukqXe5w)
- [SSL, TLS, HTTP, HTTPS Explained](https://yewtu.be/watch?v=hExRDVZHhig)
- [IP Address - IPv4 vs IPv6 Tutorial](https://yewtu.be/watch?v=ThdO9beHhpA)
