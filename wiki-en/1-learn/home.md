# Learn

This section contains links to articles, sites and conferences that explain the necessity of privacy and decentralization. You can find general information and vulgarization, but also very specific documentation on some topics.
