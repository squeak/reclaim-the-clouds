# Propaganda


## A summary of websites that communicate problems and solutions

- Why not starting by listening to people who build those surveillance system explain themselves what they do: [Cambridge Analytica - The Power of Big Data and Psychographics (or how social media can be used to manipulate political elections)](https://yewtu.be/watch?v=n8Dd5aVXLCc).
- Why you should [stop using facebook](https://www.stopusingfacebook.co/)! (hope you already don't ;))
- Every day, a new facebook scandal, see them listed [here](https://dayssincelastfacebookscandal.com/).
- [How interactions via technology are bad for social interactions](https://socialcooling.fr/).
- Brief and neat explanation on why [we shouldn't think algorithms and technology are neutral](https://www.mathwashing.com/).
- See [a practical demo](https://clickclickclick.click/) of how your browser can be tracked, and [test your browser fingerprint](https://panopticlick.eff.org/).
- The [problem with mobile phones](https://ssd.eff.org/en/module/problem-mobile-phones), also available in [french](https://ssd.eff.org/fr/module/le-probl%C3%A8me-avec-les-t%C3%A9l%C3%A9phones-portables) and many other languages.
- [About digital shadows and data gathering.](https://myshadow.org)
- [Code is law](https://www.harvardmagazine.com/2000/01/code-is-law-html) written by Lawrence Lessig in 2000 — Aussi traduit en français: [Le code fait loi](https://framablog.org/2010/05/22/code-is-law-lessig/)


## Sites to have lots of general info, learn and also find alternatives
 - [restoreprivacy](https://restoreprivacy.com/)
 - [prism-break](https://prism-break.org/en/)
 - [privacy tools](https://www.privacytools.io/)
 - [privacy guides](https://privacyguides.org)
 - [surveillance self-defense](https://ssd.eff.org/en). — *available in many languages*
 - [raddle](https://raddle.me/wiki/privacy), a very complete documentation on why privacy is something we all care about, many references and proposition of tools

In french:
 - [Libère ton ordi](http://blog.liberetonordi.com/).
 - Le [MOOC chatons](https://mooc.chatons.org/), un parcours de formation visant à apporter un regard critique et des prises de conscience quant au web auquel les géants du numérique veulent assigner la société.
 - [Le wiki d'herminien](https://wiki.pcet.link/), un site créé par un enthousiaste qui explique bien les problèmes liés à la technologie, et solutions potentielles.
 - [Nothing2hide](http://nothing2hide.org/fr/), une structure associative qui s’est donnée comme objectif d’offrir aux journalistes, avocats, militants des droits de l’homme, "simples" citoyens, les moyens de protéger leurs informations. - [*also in english*](http://nothing2hide.org/en/)
