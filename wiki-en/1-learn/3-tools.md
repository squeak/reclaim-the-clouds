# Learn about specific tools


## Emails
- [Email self defense](https://emailselfdefense.fsf.org/en), *[french version](https://emailselfdefense.fsf.org/fr/index.html)* — *also available in many other languages*
- [Some figures about emails](https://www.arobase.org/actu/chiffres-email.htm) — *french*


## Chatting
- [comparison of various messaging apps](https://www.securemessagingapps.com/)


## Investigating how traceable is your browser
- [ipleak](https://ipleak.net/), [panopticlick](https://panopticlick.eff.org/), [webbrowsertools](https://webbrowsertools.com/)
- [prevent browser to access system fonts](https://superuser.com/questions/292666/how-to-disable-permission-to-read-system-fonts-and-browser-plugin-details-in), and [an article about the issue](https://www.ghacks.net/2016/12/28/firefox-52-better-font-fingerprinting-protection/)
- [an article about browser fingerprinting](https://restoreprivacy.com/browser-fingerprinting/)
- [improving firefox privacy](https://restoreprivacy.com/firefox-privacy/)


## Search engines
- [a list of interesting search engines](https://restoreprivacy.com/private-search-engine/)


## Cloud and documents
- [why and how Nextcloud builds a private cloud software](https://archive.fosdem.org/2019/schedule/event/collab_pce/)


## Creating good passwords and passphrases
To generate [a good password](https://ssd.eff.org/en/module/creating-strong-passwords), a good approach is to [choose a passphrase with random words](https://www.eff.org/dice). You can find [here a list of random words](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases). It's also good to create random password for each site/tool you use and keep them in [a password manager](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online).
To learn more about [how easy it is to predict the passwords you could choose](http://people.ischool.berkeley.edu/~nick/aaronson-oracle/).
And here is a [privacy tools article on passwords](https://www.privacytools.io/software/passwords/).
