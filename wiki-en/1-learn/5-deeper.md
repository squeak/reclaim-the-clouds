# More in depth


## Cryptography
- [An article on how NSA is able to break encrypted communications at massive scale.](https://freedom-to-tinker.com/2015/10/14/how-is-nsa-breaking-so-much-crypto/)


## TOR (The Onion Router)
- [What is onion routing](https://invidious.fdn.fr/watch?v=QRYzre4bf7I)
- [TOR Hidden Services](https://invidious.fdn.fr/watch?v=lVcbq_a5N9I)


## Tracking users while they browse, next generations
- [Server side tagging, the new system of Google to avoid needing consent to track people.](https://www.pixeldetracking.com/fr/google-tag-manager-server-side-tagging)
- [The facebook version to track people without their consent.](https://www.pixeldetracking.com/fr/les-signaux-resilients-de-facebook-ou-comment-la-surveillance-sadapte)


## Problematic companies
- [How zoom tracks it's users while saying it doesn't.](https://arstechnica.com/tech-policy/2021/08/zoom-to-pay-85m-for-lying-about-encryption-and-sending-data-to-facebook-and-google)


## It looks like science fiction, but it actually exists
- [How a conversation can be listened just by observing the status led of a loudspeaker.](https://bxl.indymedia.org/Tech-Une-conversation-peut-etre-ecoutee-en-observant-le-voyant-d-alimentation-d-un-haut-parleur?lang=fr) — *french*
